# Helm-git debugging

This Repository is for testing a bug reported on Github. Gitlab seams to be the problem so the repo represents a testing env.

See the actual Bugreport for more information: [Bug: using ref with Gitlab only works without tag-message #264](https://github.com/aslafy-z/helm-git/issues/264)